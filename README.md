# hfmt21

Presentation and resources of the lecture for HFMT21A (German).

## Urheberrecht & Lizenzen

Der Programmcode steht unter der [`MIT-Lizenz`](./LICENSE). Die Texte und Bilder stehen unter der [Creative Commons BY NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode) Lizenz.

Für den Programmcode und die Inhalte von Dritten gelten deren Lizenen.
