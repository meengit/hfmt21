# Lizenzierung von Software


## Grundlegendes

* Urheberschaft
* Welche Rechte werden abgetreten?
  * Vertrieb
  * Nutzung
  * Veränderung
  * ...


## Begrifflichkeiten


### EULA

End User License Agreement EULA (nicht zu verwecheln mit dem Service Level Agreement SLA)


### Proprietäre Software (Lizenzen)

_Proprietäre Software bezeichnet eine Software, die das Recht und die Möglichkeiten der Wieder- und Weiterverwendung sowie Änderung und Anpassung durch Nutzer und Dritte stark einschränkt. (Wikipedia, 2021)_

Beispiel: [Microsoft 365](https://www.microsoft.com/de-ch/microsoft-365), bisher Microsoft Office 365 (Microsoft, o. J.)


### Freeware

_Freeware von englisch free „kostenlos“ und ware „Ware“) bezeichnet im allgemeinen Sprachgebrauch Software, die vom Urheber zur kostenlosen Nutzung zur Verfügung gestellt wird. Freeware ist meistens proprietär und steht damit laut der Free Software Foundation im Gegensatz zu Freier Software (englisch „free software“), die weitläufigere Freiheiten gewährt, etwa Veränderungen an der Software. Die Programmierer verzichten bei Freeware nur auf eine Nutzungsvergütung, aber nicht auf das Urheberrecht. Den Benutzern wird nur ein Nutzungsrecht eingeräumt; Änderungen der Software oder die Nutzung der Teile des Programms (wie etwa Codeschnipsel) werden untersagt. (Wikipedia, 2021)_

Beispiel: [PDF24 Creator](https://de.pdf24.org) ( geek Software GmbH, o. J.)


#### Halbfreie Software

_Halbfreie Software (englisch semi-free software) ist Software, die von Privatpersonen kostenfrei zu nichtkommerziellen Zwecken benutzt, kopiert, modifiziert und weitergeben werden kann, deren kommerzielle Nutzung jedoch an Einschränkungen gebunden ist. Die Einschränkungen können je nach Lizenz erheblich oder marginal sein. (Wikipedia, 2021)_


### Freie Software

_Freie Software (freiheitsgewährende Software, englisch free software oder auch libre software) bezeichnet Software, die die Freiheit von Computernutzern in den Mittelpunkt stellt. Freie Software wird dadurch definiert, dass ein Nutzer mit dem Empfang der Software die Nutzungsrechte mitempfängt und diese ihm nicht vorenthalten oder beschränkt werden. (Wikipedia, 2021)_

Beispiel: [VSCodium](https://vscodium.com/) (The VSCodium contributors et al., o. J.)


<img src="https://upload.wikimedia.org/wikipedia/commons/1/10/Konzept-karte_der_Freien_Software.svg" alt="Übersicht freie Software" style="width:auto;height:480px;" >

<small>Übersicht freie Software (Wikipedia, 2021)</small>


### Open Source (≠ freie Software!)

_Als Open Source (aus englisch open source, wörtlich offene Quelle) wird Software bezeichnet, deren Quelltext öffentlich und von Dritten eingesehen, geändert und genutzt werden kann. Open-Source-Software kann meistens kostenlos genutzt werden. (Wikipedia, 2021)_

Beispiel: [Microsoft VSCode Source Code](https://github.com/microsoft/vscode) (Microsoft Corporation, 2015/2021) und [VSCodium Source Code](https://github.com/VSCodium/vscodium) (The VSCodium contributors et al., 2018/2021)


## Lizenztypen für freie Software

MIT, GNU GPLv3, Mozilla Public License, Apache License – siehe [«Comparison of free and open-source software licences»](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licences) (Wikipedia, 2021)
