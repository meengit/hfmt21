# Management-Spiel


## Die Kompetenzsteigerungsspirale


<iframe width="1120" height="560" src="https://www.youtube.com/embed/zpgizaftqwI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<small>Peter Sloterdijk - Das Zeug zur Macht ( Kapitale Berlin, 2012)</small>


## Aufgabe

Fassen sie den Vortrag von Peter Sloterdijk in drei Sätzen zusammen. (5 Minuten)


## Präsentation

Jeder Student präsentiert seine drei Sätze.


## Bewertung

* Vergeben sie pro Präsentation 1, 2 oder 3 Punkte
* Sie dürfen 1x 4 Punkte vergeben
* ***Optional: Sie dürfen 1x 5 Punkte vergeben, wenn sie bereits 1x 4 Punkte vergeben haben***


## Auflösung

* «Sie können nicht alles wissen.»
* Versuchen sie die [Informationsarchitektur](https://informationstechnologien.readthedocs.io/de/latest/pages/cms.html#informationsarchitektur) (Eberhard, 2021) zu verstehen
  * Zusammmenhänge
  * Abhängigkeiten
  * Workflow
  * Kosten (nicht überbewerten)


